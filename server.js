const express = require('express')
const session = require('express-session')
const server = express()

const bodyParser = require('body-parser')
const multer = require('multer');
const upload = multer();

const serverHost = '127.0.0.1'
const serverPort = 8080

const Helpers = require('./app/helpers')

const Plex = require('./app/plex')
const User = require('./app/user')

server.set('view engine', 'pug')
server.set('views', './views')

// FORCE TRAILING SLASH ON URL
server.use((req, res, next) => {
  const test = /([^\/][(\w\s-_)]+.(js|css|jpg|png|gif|svg|eot|ttf|woff|woff2))$/.test(req.url);
  if (req.url.substr(-1) !== '/' && req.url.length > 1 && !test)
    res.redirect(301, `${req.url}/`)
  else
    next();
});

server.use(session({ secret: 'i make ducks dance easily', cookie: { maxAge: 60000 }}))
server.use(bodyParser.json())
server.use(bodyParser.urlencoded({ extended: true }))
server.use(upload.array())
server.use(express.static('public'))

server.locals = {
  Title: "SINDISTRICT.TV",
  Author: "Sebastian Inman",
  Description: "My app's description",
  Plex: {
    Config: Plex.Config,
    Token: Plex.Token,
    Collections: Plex.FetchCollections(),
    Movies: Plex.FetchMovies(),
    Shows: Plex.FetchShows(),
    Genres: Plex.FetchGenres()
  }
}

server.route('/').get((req, res) => {

  res.render('dashboard', {
    Content: {
      Deck: Plex.FetchDeck()
    }
  })

  // if (req.session.authenticated) {
  
  //   res.render('dashboard', { title: Plex.Config.ServerName, user: req.session.user, data: JSON.stringify(Plex.FetchLibrary()) })

  // } else {

  //   res.render('login', { title: Plex.Config.ServerName, message: 'Please sign in' })

  // }

}).post((req, res) => {

  User.Login(req, res);

})


server.route('/new/').get((req, res) => {

  // server.locals.Plex.Collections = Plex.FetchCollections()

  // res.render('new', {
  //   Collection: {Title: 'Recently Added', Slug: 'new'},
  //   Title: 'Recently Added', 
  //   Library: server.locals.Plex.Collections
  // })

})


server.route('/collections/').get((req, res) => {

  server.locals.Plex.Collections = Plex.FetchCollections()

  res.render('collections', {
    Collection: {Title: 'Collections', Slug: 'collections'},
    Title: 'Collections', 
    Library: server.locals.Plex.Collections
  })

})


server.route('/movies/').get((req, res) => {

  server.locals.Plex.Movies = Plex.FetchMovies()

  res.render('discover', {
    Collection: {Title: 'Movies', Slug: 'movies'},
    Title: 'Discover Movies', 
    Library: server.locals.Plex.Movies
  })

})


server.route('/movies/:movie/').get((req, res) => {

  res.render('watch/movie', {
    Title: server.locals.Plex.Movies[req.params.movie].title,
    Movie: server.locals.Plex.Movies[req.params.movie]
  })

})


server.route('/shows/').get((req, res) => {

  server.locals.Plex.Shows = Plex.FetchShows()

  res.render('discover', {
    Collection: {Title: 'TV Shows', Slug: 'shows'},
    Title: 'Discover TV Shows', 
    Library: server.locals.Plex.Shows
  })

})


server.route('/shows/:show/').get((req, res) => {

  res.render('watch/show', {
    Title: server.locals.Plex.Shows[req.params.show].title,
    Show: server.locals.Plex.Shows[req.params.show]
  })

})


server.route('/logout/').get((req, res) => {

  User.Logout(req, res)

})

server.listen(serverPort, serverHost, () => console.log(`Web server listening at ${serverHost}:${serverPort}`))
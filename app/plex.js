const package = require('../package.json');

const StripColor = require('dominant-color')
const TinyColor = require('tinycolor2')

const base64Img = require('base64-img')

const PlexAPI = require('plex-api')
const PlexAuth = require('plex-api-credentials')

const Helpers = require('./helpers')

const PlexToken = 'PByvB6yRE1FswVPzbSZc'

const PlexConfig = {
  serverName: 'SINDISTRICT.TV',
  hostname: '131.150.85.237',
  port: '23963',
  options: {
    identifier: 'sindistrict.tv.website',
    deviceName: 'SinDistrict Website',
    product: 'https://sindistrict.tv',
    version: package.version,
    device: ' ',
    platform: 'Web'
  }
}

let PlexAdmin = new PlexAPI(Object.assign({}, PlexConfig, { token: PlexToken }))

module.exports = {



  Token: PlexToken,
  Config: PlexConfig,

  Collections: {},
  Movies: {},
  Shows: {},
  Genres: {},



  /**
   * @method Plex.FetchMovies
   * @param [match] string
   *    @value 'all' Returns all movies in the collection.
   *    @value 'unwatched' Returns all movies unwatched by the user.
   *    @value 'newest' Returns the movies most recently released.
   *    @value 'recentlyAdded' Returns the movies most recently added to the server.
   *    @value 'recentlyViewed' Returns the most recently viewed movies on the server.
   */

  FetchCollections: () => {

    PlexAdmin.query(`/library/sections/1/collection`).then(response => {

      Object.keys(response.MediaContainer.Directory).map(key => {

        let CollectionTitle = response.MediaContainer.Directory[key].title

        module.exports.Collections[CollectionTitle] = {};

        PlexAdmin.query(`/library/sections/1/collection/${response.MediaContainer.Directory[key].key}`).then(collection => {

          Object.keys(collection.MediaContainer.Metadata).map(key => {

            let MediaSlug = Helpers.SlugifyString(collection.MediaContainer.Metadata[key].title)

            module.exports.Collections[CollectionTitle][MediaSlug] = module.exports.Movies[MediaSlug];

          })

        }, err => { console.log(err) })

      })

    }, err => { console.log(err) })

    return module.exports.Collections

  },



  /**
   * @method Plex.FetchMovies
   * @param [match] string
   *    @value 'all' Returns all movies in the collection.
   *    @value 'unwatched' Returns all movies unwatched by the user.
   *    @value 'newest' Returns the movies most recently released.
   *    @value 'recentlyAdded' Returns the movies most recently added to the server.
   *    @value 'recentlyViewed' Returns the most recently viewed movies on the server.
   */

  FetchMovies: (key='1', match='all') => {

    PlexAdmin.query(`/library/sections/${key}/${match}`).then(response => {

      Object.keys(response.MediaContainer.Metadata).map(key => {

        let MediaSlug = Helpers.SlugifyString(response.MediaContainer.Metadata[key].title)

        module.exports.Movies[MediaSlug] = response.MediaContainer.Metadata[key]
        module.exports.Movies[MediaSlug].Color = 'transparent'

        module.exports.Movies[MediaSlug].Poster = module.exports.MediaPoster(module.exports.Movies[MediaSlug])
        module.exports.Movies[MediaSlug].Banner = {
          Small: module.exports.MediaBanner(module.exports.Movies[MediaSlug], 400),
          Large: module.exports.MediaBanner(module.exports.Movies[MediaSlug], 1600)
        }

        StripColor(module.exports.Movies[MediaSlug].Banner.Small, (err, color) => {

          module.exports.Movies[MediaSlug].Color = TinyColor(`#${color}`).lighten(15).saturate(50).toHexString()

        })

        // base64Img.requestBase64(module.exports.Movies[MediaSlug].Poster, (err, res, base64) => {

        //   module.exports.Movies[MediaSlug].Poster = base64

        // })

        // base64Img.requestBase64(module.exports.Movies[MediaSlug].Banner.Small, (err, res, base64) => {

        //   module.exports.Movies[MediaSlug].Banner.Small = base64

        // })

        // base64Img.requestBase64(module.exports.Movies[MediaSlug].Banner.Large, (err, res, base64) => {

        //   module.exports.Movies[MediaSlug].Banner.Large = base64

        // })

      })

    }, err => { console.log(err) })

    return module.exports.Movies

  },



  /**
   * @method Plex.FetchShows
   * @param [match] string
   *    @value 'all' Returns all movies in the collection.
   *    @value 'unwatched' Returns all movies unwatched by the user.
   *    @value 'newest' Returns the movies most recently released.
   *    @value 'recentlyAdded' Returns the movies most recently added to the server.
   *    @value 'recentlyViewed' Returns the most recently viewed movies on the server.
   */

  FetchShows: (key='2', match='all') => {

    PlexAdmin.query(`/library/sections/${key}/${match}`).then(response => {

      Object.keys(response.MediaContainer.Metadata).map(key => {

        let MediaSlug = Helpers.SlugifyString(response.MediaContainer.Metadata[key].title)

        module.exports.Shows[MediaSlug] = response.MediaContainer.Metadata[key]
        module.exports.Shows[MediaSlug].Color = 'transparent'

        module.exports.Shows[MediaSlug].Poster = module.exports.MediaPoster(module.exports.Shows[MediaSlug])
        module.exports.Shows[MediaSlug].Banner = {
          Small: module.exports.MediaBanner(module.exports.Shows[MediaSlug], 600),
          Large: module.exports.MediaBanner(module.exports.Shows[MediaSlug], 1600)
        }

        StripColor(module.exports.Shows[MediaSlug].Poster, (err, color) => {

          module.exports.Shows[MediaSlug].Color = TinyColor(`#${color}`).lighten(10).saturate(40).toHexString()

        })

      })

    }, err => { console.log(err) })

    return module.exports.Shows

  },



  /**
   * @method Plex.FetchGenres
   * @param [match] string
   *    @value 'all' Returns all movies in the collection.
   *    @value 'unwatched' Returns all movies unwatched by the user.
   *    @value 'newest' Returns the movies most recently released.
   *    @value 'recentlyAdded' Returns the movies most recently added to the server.
   *    @value 'recentlyViewed' Returns the most recently viewed movies on the server.
   */

  FetchGenres: () => {

    // if(Helpers.ObjectIsEmpty(module.exports.Movies)) module.exports.Movies = module.exports.FetchMovies();
    // if(Helpers.ObjectIsEmpty(module.exports.Shows)) module.exports.Movies = module.exports.FetchShows();

    // console.log('fetching genres...')
    // console.log(module.exports.Movies)

    // let genres = {};

    // Object.keys(Movies).map(movie => {

    //   console.log(movie);

    // })

    return module.exports.Genres;

  },



  FetchDeck: () => {

    let Deck

    PlexAdmin.query(`/library/onDeck`).then(response => {

      Deck = response

    }, err => { console.log(err) })

    return Deck

  },


  MediaPoster: (key, size = 600) => {

    return `http://${PlexConfig.hostname}:${PlexConfig.port}/photo/:/transcode?url=${key.thumb}&width=${size}&height=${size}&X-Plex-Token=${PlexToken}`

  },


  MediaBanner: (key, size = 600) => {

    return `http://${PlexConfig.hostname}:${PlexConfig.port}/photo/:/transcode?url=${key.art}&width=${size}&height=${size}&X-Plex-Token=${PlexToken}`

  },


  SearchLibrary: (term) => {

  }

}
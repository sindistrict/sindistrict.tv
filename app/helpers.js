module.exports = {

  SlugifyString: string => {

  return string.toString().toLowerCase()
    .replace(/\s+/g, '-')
    .replace(/[^\w\-]+/g, '')
    .replace(/\-\-+/g, '-')
    .replace(/^-+/, '')
    .replace(/-+$/, '');

  },


  ObjectIsEmpty: obj => {

    return Object.entries(obj).length === 0 && obj.constructor === Object;

  }


}
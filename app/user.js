const Plex = require('./plex')

module.exports = {

  Login: (req, res) => {

    if(req.body.method === 'login' && req.body.username && req.body.password) {

      if(account = Plex.IsValidUser(req.body.username, req.body.password)) {

        req.session.authenticated = account.authtoken;
        req.session.user = account.username;
        res.redirect('/');

      }else{

        res.render('login', { title: Plex.Config.ServerName, message: 'Please sign in', error: `Sorry, looks like you don't have access to this server.` })

      }

    }else{

      res.render('login', { title: Plex.Config.ServerName, message: 'Please sign in', error: `Something went wrong, please try again.` })

    }

  },

  Logout: (req, res) => {

    req.session.destroy(err => {

      res.redirect('/')

    })

  }

}